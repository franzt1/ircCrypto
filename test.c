/*

  Copyright (c) 2010-2015 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fish.h"
#include "keystore.h"

// We can't use the XChat plugin API from here...
// Note that this function is used from keystore.c
gchar *get_config_filename() {
    const gchar *homedir = g_get_home_dir();
    return g_build_filename(homedir, ".config", "hexchat", "blow.ini", NULL);
}


static bool prompt(const char *message, bool def) {
    while (1) {
        char resp[100];
        fprintf(stderr, "%s [%s] ", message, def ? "Y/n" : "y/N");
        if (!fgets(resp, sizeof(resp), stdin)) { abort(); }
        if (resp[0] == '\0' || resp[0] == '\n' || resp[0] == '\r') {
            return def;
        }
        if (resp[0] == 'Y' || resp[0] == 'y') return true;
        if (resp[0] == 'N' || resp[0] == 'n') return false;
        fprintf(stderr, "Please answer Y or N.");
    }
}


static int decrypt(int nick_count, char *nicks[]) {
    char encrypted[8192];
    while (fgets(encrypted, sizeof(encrypted), stdin)) {
        char *msg;
        for (int i = 0; i < nick_count; i++) {
            msg = fish_decrypt_from_nick(nicks[i], encrypted);
            if (msg) goto success;
        }
        fprintf(stderr, "None of the recipients were found in the key store!\n");
        return 1;
      success:
        fprintf(stderr, "Decrypted text >>>%s<<<\n", msg);
        free(msg);
    }
    return 0;
}

static int encrypt(int nick_count, char *nicks[]) {
    char message[8192];
    while (fgets(message, sizeof(message), stdin)) {
        // Remove newline character
        char *newline = strchr(message, '\n');
        if (newline) *newline = '\0';
        
        bool error = false;
        for (int i = 0; i < nick_count; i++) {
            char *encrypted = fish_encrypt_for_nick(nicks[i], message);
            if (encrypted) {
                fprintf(stderr, "Encrypted [%s]:  >>>%s<<<\n", nicks[i], encrypted);
                free(encrypted);
            } else {
                error = true;
            }
        }
        
        if (error) {
            fprintf(stderr, "Some of the recipients were't found in the key store!\n");
            return 1;
        }
    }
    return 0;
}

static int setkeys(int nick_count, char *nicks[]) {
    fprintf(stderr, "NOTE: Passphrases are NOT hidden!\n");
    for (int i = 0; i < nick_count; i++) {
        char password[8192];
        fprintf(stderr, "Enter passphrase for %s: ", nicks[i]);
        
        if (!fgets(password, sizeof(password), stdin)) {
            fprintf(stderr, "Cancelled.\n");
            return 1;
        }
        
        char *newline = strchr(password, '\n');
        if (newline) *newline = '\0';
        
        fprintf(stderr, "Older plugins/clients only support the insecure ECB mode.\n");
        bool use_cbc = prompt("Enable encryption in the more secure CBC mode by default?", 1);
        
        if (!keystore_store_key(nicks[i], password, use_cbc)) {
            fprintf(stderr, "Failed to update key file, for nick %s.\n", nicks[i]);
            return 1;
        }
    }
    return 0;
}

static int setmode(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "-m option requires a mode argument (CBC or ECB) and at least one nick name\n");
        return 2;
    }
    const char *mode = argv[0];
    bool is_cbc;
    if (!strcmp(mode, "ecb") || !strcmp(mode, "ECB")) {
        is_cbc = false;
    } else if (!strcmp(mode, "cbc") || !strcmp(mode, "CBC")) {
        is_cbc = true;
    } else {
        fprintf(stderr, "Invalid mode, must be CBC or ECB\n");
        return 2;
    }
    
    int error = 0;
    for (int i = 1; i < argc; i++) {
        if (!keystore_set_mode(argv[i], is_cbc)) {
            fprintf(stderr, "Failed to set mode for nick %s.\n", argv[i]);
            error = 1;
        }
    }
    return error;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "usage: %s [-e] nick...\n"
                        "       %s -k nick...\n"
                        "       %s -m MODE nick...\n"
                        "\n"
                        "Options:\n"
                        "  -e       Encrypt (default is to decrypt)\n"
                        "  -k       Set key (will prompt for password)\n"
                        "  -m MODE  Set default encryption mode for key.\n"
                        "           Supported modes: ecb (default), cbc (recommended)\n"
                        "\n"
                        "This tool will read/write keys from %s\n",
                        argv[0], argv[0], argv[0], get_config_filename());
        return 2;
    }
    
    
    if (strcmp(argv[1], "-k") == 0) {
        return setkeys(argc-2, &argv[2]);
    } else if (strcmp(argv[1], "-m") == 0) {
        return setmode(argc-2, &argv[2]);
    } else if (strcmp(argv[1], "-e") == 0) {
        return encrypt(argc-2, &argv[2]);
    } else {
        return decrypt(argc-1, &argv[1]);
    }
}


